/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package administrador;

import java.sql.*;

//Patrón de diseño Singleton//
//Resuelve el problema de diversas instancias de un objeto para  evitar sobrecarga del heap.
public class conexion {
    private static Connection cnx = null;
    public static Connection obtener() throws SQLException, ClassNotFoundException {
       if (cnx == null) {
          try {
             Class.forName("com.mysql.jdbc.Driver");
             cnx = DriverManager.getConnection("jdbc:mysql://35.190.160.177/tickets", "android", "android");
          } catch (SQLException ex) {
             throw new SQLException(ex);
          } catch (ClassNotFoundException ex) {
             throw new ClassCastException(ex.getMessage());
          }
       }
       return cnx;
    }
    public static void cerrar() throws SQLException {
       if (cnx != null) {
          cnx.close();
       }
    }
}
